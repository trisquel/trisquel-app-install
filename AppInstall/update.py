import sys
import glob
import os
import os.path
import re
import sys

from optparse import OptionParser

import dbm.gnu

os.unsetenv("DISPLAY")

try:
    import xdg.DesktopEntry
except ImportError as e:
    print("WARNING: can not import xdg.DesktopEntry, aborting")
    sys.exit(0)

try:
    from .CoreMenu import *
except ImportError as e:
    print("Can't import AppInstall.CoreMenu, aborting")
    sys.exit(0)


def generate_mime_map(desktop_dir, cache_dir):
    dicts = {'mime': {}}

    def record_provider(de, cp, defield, dictname):
        try:
            keys = de.get(defield, list=True)
        except keyError:
            return
        if not keys:
            return
        dict = dicts[dictname]
        for key in keys:
            try:
                l = dict[key]
            except KeyError:
                l = []
                dict[key] = l
            l.append(cp)

    for fn in glob.glob(os.path.join(desktop_dir, 'desktop/*.desktop')):
        try:
            de = xdg.DesktopEntry.DesktopEntry(fn)
        except Exception as e:
            print("bad .desktop file: %s: %s" % (fn, e), file=sys.stderr)
        try:
            component = de.get('X-AppInstall-Section')
            package = de.get('X-AppInstall-Package')
        except KeyError:
            continue
        cp = component+"/"+package
        record_provider(de, cp, 'MimeType', 'mime')

    for (dictname, dict) in dicts.items():
        g = dbm.gnu.open(os.path.join(cache_dir,
                                   "gai-"+dictname+"-map.gdbm"), 'nfu')
        for (key, l) in dict.items():
            g[key] = ' '.join(l)
        g.sync()
        g.close()
        os.chmod(os.path.join(cache_dir, "gai-"+dictname+"-map.gdbm"), 0o644)


def generate_menu_cache(desktop_dir, cache_dir):
    # the regular menu cache
    menu = CoreApplicationMenu(desktop_dir)
    menu.createMenuCache(cache_dir)
    # now the mime menu_cache
    mime_pickle = {}
    icons_pickle = {}
    for cat in menu.pickle:
        for item in menu.pickle[cat]:
            if item.mime != []:
                if cat not in mime_pickle:
                    mime_pickle[cat] = []
                mime_pickle[cat].append(item)
            if item.iconname != '':
                icons = icons_pickle.get(item.pkgname) or []
                if not item.iconname in icons:
                    icons.append(item.iconname)
                icons_pickle[item.pkgname] = icons
    pickle.dump(mime_pickle, open('%s/mime_menu.p' % cache_dir, 'wb'), 2)
    os.chmod('%s/mime_menu.p' % cache_dir, 0o644)
    pickle.dump(icons_pickle, open('%s/pkgname_icons.p' % cache_dir, 'wb'), 2)
    os.chmod('%s/pkgname_icons.p' % cache_dir, 0o644)


def main():
    parser = OptionParser()
    parser.add_option("-d", "--desktop-dir", action="store",
                      dest="desktop_dir",
                      default="/usr/share/app-install",
                      help="Directory that contains the desktop files "
                      "of the applications")
    parser.add_option("-c", "--cache-dir", action="store",
                      dest="cache_dir",
                      default="/var/cache/app-install",
                      help="Directory where the data should be cached in")
    (options, args) = parser.parse_args()
    for path in (options.desktop_dir, options.cache_dir):
        if not os.path.isdir(path):
            print("%s is not a valid directory" % path)
            sys.exit(1)
    print("Caching application data...")
    try:
        generate_menu_cache(options.desktop_dir, options.cache_dir)
        print("Generating mime maps...")
        generate_mime_map(options.desktop_dir, options.cache_dir)
    except IOError:
        print("You must run this program with administrator privileges."\
            "(eg. sudo update-app-instal)")
