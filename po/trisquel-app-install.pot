# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#: ../AppInstall/distros/Ubuntu.py:191
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-14 16:36+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../data/trisquel-app-install.ui.h:1
msgid "_Reload"
msgstr ""

#: ../data/trisquel-app-install.ui.h:2
msgid ""
"<big><b>The list of available applications is out of date</b></big>\n"
"\n"
"To reload the list you need a working internet connection."
msgstr ""

#: ../data/trisquel-app-install.ui.h:5
msgid "_Remove"
msgstr ""

#: ../data/trisquel-app-install.ui.h:6
msgid "applications"
msgstr ""

#: ../data/trisquel-app-install.ui.h:7
msgid "<b>Add</b>"
msgstr ""

#: ../data/trisquel-app-install.ui.h:8
msgid "<b>Remove</b>"
msgstr ""

#: ../data/trisquel-app-install.ui.h:9
#: ../data/trisquel-app-install.desktop.in.h:1
#: ../data/trisquel-app-install-xfce.desktop.in.h:1
msgid "Add/Remove Applications"
msgstr ""

#: ../data/trisquel-app-install.ui.h:10
msgid "The categories represent the application menu"
msgstr ""

#: ../data/trisquel-app-install.ui.h:11
msgid "Categories"
msgstr ""

#: ../data/trisquel-app-install.ui.h:12
msgid "Show:"
msgstr ""

#: ../data/trisquel-app-install.ui.h:13
msgid "Search:"
msgstr ""

#: ../data/trisquel-app-install.ui.h:14
msgid "Cancel pending changes and close the window"
msgstr ""

#: ../data/trisquel-app-install.ui.h:15
msgid "_Apply Changes"
msgstr ""

#: ../data/trisquel-app-install.ui.h:16
msgid "Apply pending changes and close the window"
msgstr ""

#: ../data/trisquel-app-install.ui.h:17
msgid "<big><b>Loading...</b></big>"
msgstr ""

#: ../data/trisquel-app-install.ui.h:18
msgid "_Add/Remove More Applications"
msgstr ""

#: ../data/trisquel-app-install.ui.h:19
msgid "_Retry"
msgstr ""

#: ../data/trisquel-app-install.desktop.in.h:2
#: ../data/trisquel-app-install-xfce.desktop.in.h:2
msgid "Package Manager"
msgstr ""

#: ../data/trisquel-app-install.desktop.in.h:3
#: ../data/trisquel-app-install-xfce.desktop.in.h:3
msgid "Install and remove applications"
msgstr ""

#: ../AppInstall/activation.py:143
msgid "no suitable application"
msgstr ""

#: ../AppInstall/activation.py:144
msgid ""
"No application suitable for automatic installation is available for handling "
"this kind of file."
msgstr ""

#: ../AppInstall/activation.py:148
msgid "no application found"
msgstr ""

#: ../AppInstall/activation.py:149
msgid "No application is known for this kind of file."
msgstr ""

#: ../AppInstall/activation.py:166 ../AppInstall/activation.py:367
msgid "Searching for appropriate applications"
msgstr ""

#: ../AppInstall/activation.py:168 ../AppInstall/activation.py:242
msgid "Please wait. This might take a minute or two."
msgstr ""

#: ../AppInstall/activation.py:197
msgid "Search for suitable codec?"
msgstr ""

#: ../AppInstall/activation.py:198
msgid ""
"The required software to play this file is not installed. You need to "
"install suitable codecs to play media files. Do you want to search for a "
"codec that supports the selected file?\n"
"\n"
"The search will also include software which is not officially supported."
msgstr ""

#: ../AppInstall/activation.py:209
msgid "Invalid commandline"
msgstr ""

#: ../AppInstall/activation.py:210
#, python-format
msgid "'%s' does not understand the commandline argument '%s'"
msgstr ""

#: ../AppInstall/activation.py:229
msgid ""
"Some countries allow patents on software, and freely redistributable "
"software like Trisquel will not distribute patent-encumbered software. If "
"you are in one of these jurisdictions, please select an alternative free "
"plug-in above to install it."
msgstr ""

#: ../AppInstall/activation.py:240
msgid "Searching for appropriate codecs"
msgstr ""

#: ../AppInstall/activation.py:245 ../AppInstall/activation.py:372
msgid "_Install"
msgstr ""

#: ../AppInstall/activation.py:246
msgid "Install Media Plug-ins"
msgstr ""

#: ../AppInstall/activation.py:250
msgid "Codec"
msgstr ""

#. TRANSLATORS: %s represents a file path
#: ../AppInstall/activation.py:332
#, python-format
msgid "\"%s\" cannot be opened"
msgstr ""

#: ../AppInstall/activation.py:369
#, python-format
msgid ""
"A list of applications that can handle documents of the type '%s' will be "
"created"
msgstr ""

#. TRANSLATORS: %s represents a file path
#: ../AppInstall/activation.py:375
#, python-format
msgid "Install applications to open \"%s\""
msgstr ""

#: ../AppInstall/activation.py:378
msgid "Install applications"
msgstr ""

#: ../AppInstall/activation.py:390
msgid "_Search"
msgstr ""

#: ../AppInstall/activation.py:430
msgid "Searching for extensions"
msgstr ""

#: ../AppInstall/activation.py:431
msgid "Extensions allow you to add new features to your application."
msgstr ""

#: ../AppInstall/activation.py:433
msgid "Install/Remove Extensions"
msgstr ""

#: ../AppInstall/activation.py:435
msgid "Extension"
msgstr ""

#: ../AppInstall/AppInstallApp.py:178
msgid ""
"To install an application check the box next to the application. Uncheck the "
"box to remove the application."
msgstr ""

#: ../AppInstall/AppInstallApp.py:181
msgid "To perform advanced tasks use the Synaptic package manager."
msgstr ""

#: ../AppInstall/AppInstallApp.py:183
msgid "Quick Introduction"
msgstr ""

#: ../AppInstall/AppInstallApp.py:230
msgid "Installed applications only"
msgstr ""

#: ../AppInstall/AppInstallApp.py:231
msgid "Show only applications that are installed on your computer"
msgstr ""

#: ../AppInstall/AppInstallApp.py:357
msgid "Error reading the addon CD"
msgstr ""

#: ../AppInstall/AppInstallApp.py:358
msgid "The addon CD may be corrupt "
msgstr ""

#: ../AppInstall/AppInstallApp.py:449
msgid "The list of applications is not available"
msgstr ""

#: ../AppInstall/AppInstallApp.py:450
msgid ""
"Click on 'Reload' to load it. To reload the list you need a working internet "
"connection. "
msgstr ""

#: ../AppInstall/AppInstallApp.py:468
#, python-format
msgid "%s cannot be installed on your computer type (%s)"
msgstr ""

#: ../AppInstall/AppInstallApp.py:471
msgid ""
"Either the application requires special hardware features or the vendor "
"decided to not support your computer type."
msgstr ""

#: ../AppInstall/AppInstallApp.py:497
#, python-format
msgid "Cannot install '%s'"
msgstr ""

#: ../AppInstall/AppInstallApp.py:498
#, python-format
msgid ""
"This application conflicts with other installed software. To install '%s' "
"the conflicting software must be removed first.\n"
"\n"
"Switch to the 'synaptic' package manager to resolve this conflict."
msgstr ""

#: ../AppInstall/AppInstallApp.py:550
#, python-format
msgid "Cannot remove '%s'"
msgstr ""

#: ../AppInstall/AppInstallApp.py:551
#, python-format
msgid ""
"One or more applications depend on %s. To remove %s and the dependent "
"applications, use the Synaptic package manager."
msgstr ""

#. Fallback
#: ../AppInstall/AppInstallApp.py:648 ../AppInstall/DialogProprietary.py:22
#: ../AppInstall/distros/Debian.py:24
#, python-format
msgid "Enable the installation of software from %s?"
msgstr ""

#: ../AppInstall/AppInstallApp.py:650
#, python-format
msgid ""
"%s is provided by a third party vendor. The third party vendor is "
"responsible for support and security updates."
msgstr ""

#: ../AppInstall/AppInstallApp.py:654 ../AppInstall/DialogProprietary.py:25
msgid "You need a working internet connection to continue."
msgstr ""

#: ../AppInstall/AppInstallApp.py:658 ../AppInstall/DialogProprietary.py:35
msgid "_Enable"
msgstr ""

#. show an error dialog if something went wrong with the cache
#: ../AppInstall/AppInstallApp.py:875
msgid "Failed to check for installed and available applications"
msgstr ""

#: ../AppInstall/AppInstallApp.py:876
msgid ""
"This is a major failure of your software management system. Please check for "
"broken packages with synaptic, check the file permissions and correctness of "
"the file '/etc/apt/sources.list' and reload the software information with: "
"'sudo apt-get update' and 'sudo apt-get install -f'."
msgstr ""

#: ../AppInstall/AppInstallApp.py:965
msgid "Apply changes to installed applications before closing?"
msgstr ""

#: ../AppInstall/AppInstallApp.py:966
msgid "If you do not apply your changes they will be lost permanently."
msgstr ""

#: ../AppInstall/AppInstallApp.py:970
msgid "_Close Without Applying"
msgstr ""

#: ../AppInstall/AppInstallApp.py:986
msgid "No help available"
msgstr ""

#: ../AppInstall/AppInstallApp.py:987
msgid "To display the help, you need to install the \"yelp\" application."
msgstr ""

#. FIXME: move this inside the dialog class, we show a different
#. text for a quit dialog and a approve dialog
#: ../AppInstall/AppInstallApp.py:1036
msgid "Apply the following changes?"
msgstr ""

#: ../AppInstall/AppInstallApp.py:1037
msgid ""
"Please take a final look through the list of applications that will be "
"installed or removed."
msgstr ""

#: ../AppInstall/AppInstallApp.py:1267
msgid "There is no matching application available."
msgstr ""

#. TRANSLATORS: %s represents a filter name
#: ../AppInstall/AppInstallApp.py:1277
#, python-format
msgid "To broaden your search, choose \"%s\"."
msgstr ""

#. TRANSLATORS: %s represents a filter name
#: ../AppInstall/AppInstallApp.py:1281
#, python-format
msgid "To broaden your search, choose \"%s\" or \"%s\"."
msgstr ""

#. TRANSLATORS: Show refers to the Show: combobox
#: ../AppInstall/AppInstallApp.py:1287
msgid "To broaden your search, choose a different \"Show\" item."
msgstr ""

#. TRANSLATORS: All refers to the All category in the left list
#: ../AppInstall/AppInstallApp.py:1293
msgid "To broaden your search, choose 'All' categories."
msgstr ""

#: ../AppInstall/BrowserView.py:99
#, python-format
msgid "Failed to open '%s'"
msgstr ""

#: ../AppInstall/CoreMenu.py:215
msgid "Applications"
msgstr ""

#: ../AppInstall/DialogComplete.py:135
msgid "Software installation failed"
msgstr ""

#: ../AppInstall/DialogComplete.py:136
msgid ""
"There has been a problem during the installation of the following pieces of "
"software."
msgstr ""

#: ../AppInstall/DialogComplete.py:139 ../AppInstall/DialogComplete.py:156
#: ../AppInstall/DialogComplete.py:173 ../AppInstall/DialogComplete.py:201
msgid "Add/Remove More Software"
msgstr ""

#: ../AppInstall/DialogComplete.py:141
msgid "Application installation failed"
msgstr ""

#: ../AppInstall/DialogComplete.py:142
msgid ""
"There has been a problem during the installation of the following "
"applications."
msgstr ""

#: ../AppInstall/DialogComplete.py:152
msgid "Software could not be removed"
msgstr ""

#: ../AppInstall/DialogComplete.py:153
msgid ""
"There has been a problem during the removal of the following pieces of "
"software."
msgstr ""

#: ../AppInstall/DialogComplete.py:158
msgid "Not all applications could be removed"
msgstr ""

#: ../AppInstall/DialogComplete.py:159
msgid ""
"There has been a problem during the removal of the following applications."
msgstr ""

#: ../AppInstall/DialogComplete.py:169
msgid "Installation and removal of software failed"
msgstr ""

#: ../AppInstall/DialogComplete.py:170
msgid ""
"There has been a problem during the installation or removal of the following "
"pieces of software."
msgstr ""

#: ../AppInstall/DialogComplete.py:175
msgid "Installation and removal of applications failed"
msgstr ""

#: ../AppInstall/DialogComplete.py:176
msgid ""
"There has been a problem during the installation or removal of the following "
"applications."
msgstr ""

#: ../AppInstall/DialogComplete.py:184
msgid "New application has been installed"
msgstr ""

#: ../AppInstall/DialogComplete.py:185
msgid "New applications have been installed"
msgstr ""

#: ../AppInstall/DialogComplete.py:190
msgid ""
"To start a newly installed application, choose it from the applications menu."
msgstr ""

#: ../AppInstall/DialogComplete.py:193
msgid "To start a newly installed application double click on it."
msgstr ""

#: ../AppInstall/DialogComplete.py:197
msgid "Software has been installed successfully"
msgstr ""

#: ../AppInstall/DialogComplete.py:198
msgid "Do you want to install or remove further software?"
msgstr ""

#: ../AppInstall/DialogComplete.py:203
msgid "Applications have been removed successfully"
msgstr ""

#: ../AppInstall/DialogComplete.py:204
msgid "Do you want to install or remove further applications?"
msgstr ""

#: ../AppInstall/DialogMultipleApps.py:32
#, python-format
msgid "Remove %s and bundled applications?"
msgstr ""

#: ../AppInstall/DialogMultipleApps.py:33
#, python-format
msgid ""
"%s is part of a software collection. If you remove %s, you will remove all "
"bundled applications as well."
msgstr ""

#: ../AppInstall/DialogMultipleApps.py:36
msgid "_Remove All"
msgstr ""

#: ../AppInstall/DialogMultipleApps.py:38
#, python-format
msgid "Install %s and bundled applications?"
msgstr ""

#: ../AppInstall/DialogMultipleApps.py:39
#, python-format
msgid ""
"%s is part of a software collection. If you install %s, you will install all "
"bundled applications as well."
msgstr ""

#: ../AppInstall/DialogMultipleApps.py:42
msgid "_Install All"
msgstr ""

#: ../AppInstall/DialogProprietary.py:24
#, python-format
msgid "%s is provided by a third party vendor."
msgstr ""

#: ../AppInstall/DialogProprietary.py:41
msgid ""
"The application comes with the following license terms and conditions. Click "
"on the 'Enable' button to accept them:"
msgstr ""

#: ../AppInstall/DialogProprietary.py:47
msgid "Accept the license terms and install the software"
msgstr ""

#: ../AppInstall/Menu.py:132
msgid "Loading cache..."
msgstr ""

#: ../AppInstall/Menu.py:137
msgid "Collecting application data..."
msgstr ""

#: ../AppInstall/Menu.py:363
msgid "Loading applications..."
msgstr ""

#. add "All" category
#: ../AppInstall/Menu.py:366 ../AppInstall/distros/Default.py:13
msgid "All"
msgstr ""

#: ../AppInstall/Menu.py:377
#, python-format
msgid "Loading %s..."
msgstr ""

#: ../AppInstall/distros/Debian.py:14 ../AppInstall/distros/Ubuntu.py:22
msgid "All available applications"
msgstr ""

#. %s is the name of the component
#: ../AppInstall/distros/Debian.py:26 ../AppInstall/distros/Default.py:24
#: ../AppInstall/distros/Ubuntu.py:39
#, python-format
msgid "%s is not officially supported with security updates."
msgstr ""

#: ../AppInstall/distros/Debian.py:28
msgid "Enable the installation of officially supported Debian software?"
msgstr ""

#. %s is the name of the application
#: ../AppInstall/distros/Debian.py:31
#, python-format
msgid ""
"%s is part of the Debian distribution. Debian provides support and security "
"updates, which will be enabled too."
msgstr ""

#: ../AppInstall/distros/Debian.py:56
#, python-format
msgid "Debian provides support and security updates for %s"
msgstr ""

#: ../AppInstall/distros/Debian.py:61
#, python-format
msgid "%s is not an official part of Debian."
msgstr ""

#: ../AppInstall/distros/Debian.py:83
msgid ""
"<big><b>Checking installed and available applications</b></big>\n"
"\n"
"Debian offers you a large variety of applications that you can install on "
"your system."
msgstr ""

#: ../AppInstall/distros/Default.py:22
#, python-format
msgid ""
"Enable the installation of software from the %s component of your "
"Distribution?"
msgstr ""

#: ../AppInstall/distros/Default.py:90
msgid "<big><b>Checking installed and available applications</b></big>\n"
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:23
msgid "All Open Source applications"
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:26
msgid "Trisquel-maintained applications"
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:27
msgid "Third party applications"
msgstr ""

#. Fallback
#: ../AppInstall/distros/Ubuntu.py:36
#, python-format
msgid "Enable the installation of software from the %s component of Trisquel?"
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:41
msgid "Enable the installaion of officially supported Trisquel software?"
msgstr ""

#. %s is the name of the application
#: ../AppInstall/distros/Ubuntu.py:44
#, python-format
msgid ""
"%s is part of the Trisquel main distribution. The Trisquel Project provides "
"support and security updates, which will be enabled too."
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:47
msgid "Enable the installation of community maintained software?"
msgstr ""

#. %s is the name of the application
#: ../AppInstall/distros/Ubuntu.py:50
#, python-format
msgid ""
"%s is maintained by the Trisquel community. The Trisquel community provides "
"support and security updates, which will be enabled too."
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:53
msgid "Enable the installation of unsupported and restricted software?"
msgstr ""

#. %s is the name of the application
#: ../AppInstall/distros/Ubuntu.py:56 ../AppInstall/distros/Ubuntu.py:112
#, python-format
msgid ""
"The use, modification and distribution of %s is restricted by copyright or "
"by legal terms in some countries."
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:89
#, python-format
msgid ""
"%s is provided by a third party vendor from the Canonical partner repository."
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:94
#, python-format
msgid ""
"The Trisquel Project provides technical support and security updates for %s"
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:107
msgid "This application is provided by the Trisquel community."
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:133
msgid "Buy Licensed Plug-ins..."
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:167
#, python-format
msgid ""
"The Trisquel developers no longer provide updates for %s in Trisquel %s. "
"Updates may be available in a newer version of Trisquel."
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:172
#, python-format
msgid ""
"The Trisquel developers provide critical updates for %(appname)s until "
"%(support_end_month_str)s %(support_end_year)s."
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:179
#, python-format
msgid ""
"The Trisquel developers do no longer provide updates for %s in Trisquel %s. "
"Updates may be available in a newer version of Trisquel."
msgstr ""

#: ../AppInstall/distros/Ubuntu.py:184
#, python-format
msgid ""
"The Trisquel developers provide critical updates supplied by the developers "
"of %(appname)s until %(support_end_month_str)s %(support_end_year)s."
msgstr ""

#. %s is the name of the package.
#: ../AppInstall/widgets/AppDescView.py:18
#, python-format
msgid "Screenshot of %s"
msgstr ""

#: ../AppInstall/widgets/AppDescView.py:35
msgid "Description"
msgstr ""

#: ../AppInstall/widgets/AppDescView.py:85
#, python-format
msgid "%s cannot be installed"
msgstr ""

#. warn that this app is not available on this plattform
#: ../AppInstall/widgets/AppDescView.py:94
#, python-format
msgid ""
"%s cannot be installed on your computer type (%s). Either the application "
"requires special hardware features or the vendor decided to not support your "
"computer type."
msgstr ""

#: ../AppInstall/widgets/AppDescView.py:105
#, python-format
msgid ""
"%s is available in the third party software channel '%s'. To install it, "
"please click on the checkbox to activate the software channel."
msgstr ""

#: ../AppInstall/widgets/AppDescView.py:115
msgid "This application is bundled with the following applications: "
msgstr ""

#: ../AppInstall/widgets/AppDescView.py:211
msgid "Screenshot"
msgstr ""

#: ../AppInstall/widgets/AppDescView.py:219
#, python-format
msgid ""
"\n"
"Homepage: %s\n"
msgstr ""

#: ../AppInstall/widgets/AppDescView.py:221
#, python-format
msgid "Version: %s (%s)"
msgstr ""

#: ../AppInstall/widgets/AppListView.py:60
msgid "Popularity"
msgstr ""

#. Application column (icon, name, description)
#: ../AppInstall/widgets/AppListView.py:84
msgid "Application"
msgstr ""
